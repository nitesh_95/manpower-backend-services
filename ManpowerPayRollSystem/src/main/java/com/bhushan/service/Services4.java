package com.bhushan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.entity.EntityPackageNovember;
import com.bhushan.repository.RepositoryInterface4;

@Service
public class Services4 {

	@Autowired
	private RepositoryInterface4 repositoryInterface;

public boolean updateNovember(EntityPackageNovember entityPackage) {
		
		EntityPackageNovember newEntity =new EntityPackageNovember();
		newEntity.setId(entityPackage.getId());
		newEntity.setPaymentstatus(entityPackage.getPaymentstatus());
		newEntity.setDateofpayment(entityPackage.getDateofpayment());
		newEntity.setEmpname(entityPackage.getEmpname());
		newEntity.setMonthlysalary(entityPackage.getMonthlysalary());
		newEntity.setCasualleave(entityPackage.getCasualleave());
		newEntity.setSickleave(entityPackage.getSickleave());
		newEntity.setWorkingdays(entityPackage.getWorkingdays());
		newEntity.setOutcomesalary(entityPackage.getOutcomesalary());
		newEntity = repositoryInterface.save(newEntity);
		return true;
	
}
public List<EntityPackageNovember> getAllN() {
	return repositoryInterface.findAll();
}

}