package com.bhushan.filedemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bhushan.filedemo.model.DBFile;

@Repository
public interface DBFileRepository extends JpaRepository<DBFile, String> {

}
