package com.bhushan.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bhushan.entity.EntityPackage;

public interface RepositoryInterface extends JpaRepository<EntityPackage, Long>{

}
