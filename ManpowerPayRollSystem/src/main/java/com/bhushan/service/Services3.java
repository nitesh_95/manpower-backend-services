package com.bhushan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.entity.EntityPackageOctober;
import com.bhushan.repository.RepositoryInterface3;

@Service
public class Services3 {

	@Autowired
	private RepositoryInterface3 repositoryInterface;

	

public boolean updateOctober(EntityPackageOctober entityPackage) {
		
		EntityPackageOctober newEntity =new EntityPackageOctober();
		newEntity.setId(entityPackage.getId());
		newEntity.setPaymentstatus(entityPackage.getPaymentstatus());
		newEntity.setDateofpayment(entityPackage.getDateofpayment());
		newEntity.setEmpname(entityPackage.getEmpname());
		newEntity.setMonthlysalary(entityPackage.getMonthlysalary());
		newEntity.setCasualleave(entityPackage.getCasualleave());
		newEntity.setSickleave(entityPackage.getSickleave());
		newEntity.setWorkingdays(entityPackage.getWorkingdays());
		newEntity.setOutcomesalary(entityPackage.getOutcomesalary());
		newEntity = repositoryInterface.save(newEntity);
		return true;
	
}
public List<EntityPackageOctober> getAllO() {
	return repositoryInterface.findAll();
}

}