package com.bhushan;

public class LoginReponse {
	private String status;
	private String reson;
	private String solid;
	private String branchname;
	private String portaltype;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getPortaltype() {
		return portaltype;
	}
	public void setPortaltype(String portaltype) {
		this.portaltype = portaltype;
	}
	public String getReson() {
		return reson;
	}
	public void setReson(String reson) {
		this.reson = reson;
	}
	public String getSolid() {
		return solid;
	}
	public void setSolid(String solid) {
		this.solid = solid;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	@Override
	public String toString() {
		return "LoginReponse [status=" + status + ", reson=" + reson + ", solid=" + solid + ", branchname=" + branchname
				+ ", portaltype=" + portaltype + "]";
	}

	
	

}
