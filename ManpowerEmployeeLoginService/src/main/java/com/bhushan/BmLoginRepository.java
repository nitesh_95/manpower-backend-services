package com.bhushan;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface BmLoginRepository extends JpaRepository<ManagerLogin, String>{
	
   @Query("from ManagerLogin where solid=?1")
   ManagerLogin gettingBmCred(String userid);
   
   @Transactional
   @Modifying
   @Query("UPDATE ManagerLogin ml SET ml.password =:newpass WHERE ml.solid =:solid")
   int update(@Param("newpass") String newpass, @Param("solid") String solid);

	 
}
