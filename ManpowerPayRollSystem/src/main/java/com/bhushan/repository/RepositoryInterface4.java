package com.bhushan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bhushan.entity.EntityPackageNovember;

@Repository
public interface RepositoryInterface4 extends JpaRepository<EntityPackageNovember, Long>{

}
