package com.bhushan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManpowerEmployeeMgmtApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManpowerEmployeeMgmtApplication.class, args);
	}

}
