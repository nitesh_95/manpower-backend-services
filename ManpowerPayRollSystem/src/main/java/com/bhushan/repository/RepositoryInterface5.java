package com.bhushan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bhushan.entity.EntityPackageDecember;

@Repository
public interface RepositoryInterface5 extends JpaRepository<EntityPackageDecember, Long>{

}
