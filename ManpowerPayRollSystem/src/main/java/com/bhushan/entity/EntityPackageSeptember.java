package com.bhushan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "payrollsystemseptember")
@Entity
public class EntityPackageSeptember {

	@Id
	@Column(name = "id")
	private long id;
	@Column(name = "empname")
	private String empname;
	@Column(name = "paymentstatus")
	private String paymentstatus;
	@Column(name = "monthlysalary")
	private String monthlysalary;
	@Column(name = "workingdays")
	private String workingdays;
	@Column(name = "casualleave")
	private String casualleave;
	@Column(name = "dateofpayment")
	private String dateofpayment;
	@Column(name = "sickleave")
	private String sickleave;
	@Column(name = "outcomesalary")
	private String outcomesalary;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public String getMonthlysalary() {
		return monthlysalary;
	}

	public void setMonthlysalary(String monthlysalary) {
		this.monthlysalary = monthlysalary;
	}

	public String getDateofpayment() {
		return dateofpayment;
	}

	public void setDateofpayment(String dateofpayment) {
		this.dateofpayment = dateofpayment;
	}


	public String getWorkingdays() {
		return workingdays;
	}

	public void setWorkingdays(String workingdays) {
		this.workingdays = workingdays;
	}

	public String getPaymentstatus() {
		return paymentstatus;
	}

	public void setPaymentstatus(String paymentstatus) {
		this.paymentstatus = paymentstatus;
	}

	public String getCasualleave() {
		return casualleave;
	}

	public void setCasualleave(String casualleave) {
		this.casualleave = casualleave;
	}

	public String getSickleave() {
		return sickleave;
	}

	public void setSickleave(String sickleave) {
		this.sickleave = sickleave;
	}

	public String getOutcomesalary() {
		return outcomesalary;
	}

	public void setOutcomesalary(String outcomesalary) {
		this.outcomesalary = outcomesalary;
	}

	@Override
	public String toString() {
		return "EntityPackage [id=" + id + ", empname=" + empname + ", paymentstatus=" + paymentstatus
				+ ", monthlysalary=" + monthlysalary + ", workingdays=" + workingdays + ", casualleave=" + casualleave
				+ ", dateofpayment=" + dateofpayment + ", sickleave=" + sickleave + ", outcomesalary=" + outcomesalary
				+ "]";
	}

	

}