package com.bhushan.service;

import org.springframework.stereotype.Component;

@Component
public interface ServiceInterface {
	public long getCountOfEntities();
}
