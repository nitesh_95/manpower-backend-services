package com.bhushan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.entity.EntityPackageSeptember;
import com.bhushan.repository.RepositoryInterface2;

@Service
public class Services2 {


	@Autowired
	private RepositoryInterface2 repositoryInterface;

public boolean updateSeptember(EntityPackageSeptember entityPackage) {
		
	EntityPackageSeptember newEntity =new EntityPackageSeptember();
		newEntity.setId(entityPackage.getId());
		newEntity.setPaymentstatus(entityPackage.getPaymentstatus());
		newEntity.setDateofpayment(entityPackage.getDateofpayment());
		newEntity.setEmpname(entityPackage.getEmpname());
		newEntity.setMonthlysalary(entityPackage.getMonthlysalary());
		newEntity.setCasualleave(entityPackage.getCasualleave());
		newEntity.setSickleave(entityPackage.getSickleave());
		newEntity.setWorkingdays(entityPackage.getWorkingdays());
		newEntity.setOutcomesalary(entityPackage.getOutcomesalary());
		newEntity = repositoryInterface.save(newEntity);
		return true;
	
}
public List<EntityPackageSeptember> getAllS() {
	return repositoryInterface.findAll();
}

}