package com.bhushan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.entity.EntityPackage;
import com.bhushan.service.Services;
import com.bhushan.util.Response;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin
public class Controller {
	@Autowired
	private Services services;

	@RequestMapping("/save")
	@ApiOperation("Create Employee")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response save(@RequestBody EntityPackage entityPackage) {
		Response res = new Response();

		try {
			res = services.saveData(entityPackage);
		} catch (Exception e) {
			System.out.println(e);
		}
		return res;

	}

	@RequestMapping(value = "/count")
	public long countEntities() {
		long count = services.countEntities();
		return count;
	}

	@RequestMapping("/getAll")
	@ApiOperation("Get All Employee")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public List<EntityPackage> getList() {
		return services.getAll();
	}

	@RequestMapping("/delete/{id}")
	@ApiOperation("Delete Employee By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response delete(@PathVariable("id") long id) {
		Response res = new Response();
		boolean deleted = services.deleteId(id);
		if (deleted) {
			res.setReason("Success");
			res.setStatus("00");
		} else {
			res.setReason("Failed");
			res.setStatus("s01");
		}
		return res;
	}

	@RequestMapping("/updateData")
	@ApiOperation("Update Employee By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response update(@RequestBody EntityPackage entityPackage) {
		Response res = new Response();
		boolean updated = services.update(entityPackage);
		if (updated) {
			res.setReason("Success");
			res.setStatus("00");
		} else {
			res.setReason("Failed");
			res.setStatus("s01");
		}
		return res;

	}

	@RequestMapping("/getEmployee/{id}")
	public ResponseEntity<EntityPackage> getArticleById(@PathVariable("id") Long id) {
		EntityPackage article = services.getArticleById(id);
		return new ResponseEntity<EntityPackage>(article, HttpStatus.OK);
	}
}