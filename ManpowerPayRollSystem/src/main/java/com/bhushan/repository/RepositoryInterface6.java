package com.bhushan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bhushan.entity.EntityPackage;

@Repository
public interface RepositoryInterface6 extends JpaRepository<EntityPackage, Long>{

}
