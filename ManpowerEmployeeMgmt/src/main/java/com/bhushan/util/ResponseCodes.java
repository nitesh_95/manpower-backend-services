package com.bhushan.util;

public enum ResponseCodes {


    SUCCESS("00", "Success"),
    FAILED("01", "Internal server error."),
    EMPTY("02", "No data found"),
    NOT_UPDATE("03","Query is Not updated"),
    NOT_CREATE("04","Unit data is not created");

    private String code;
    private String reason;

    private ResponseCodes(String code, String reason) {
        this.code = code;
        this.reason = reason;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
