package com.bhushan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bhushan.entity.EntityPackageAugust;

@Repository
public interface RepositoryInterface extends JpaRepository<EntityPackageAugust, Long>{


}
