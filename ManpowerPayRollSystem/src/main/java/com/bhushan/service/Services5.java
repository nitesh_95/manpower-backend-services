package com.bhushan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.entity.EntityPackageDecember;
import com.bhushan.repository.RepositoryInterface5;

@Service
public class Services5 {

	@Autowired
	private RepositoryInterface5 repositoryInterface;

public boolean updateDecember(EntityPackageDecember entityPackage) {
		
		EntityPackageDecember newEntity =new EntityPackageDecember();
		newEntity.setId(entityPackage.getId());
		newEntity.setPaymentstatus(entityPackage.getPaymentstatus());
		newEntity.setDateofpayment(entityPackage.getDateofpayment());
		newEntity.setEmpname(entityPackage.getEmpname());
		newEntity.setMonthlysalary(entityPackage.getMonthlysalary());
		newEntity.setCasualleave(entityPackage.getCasualleave());
		newEntity.setSickleave(entityPackage.getSickleave());
		newEntity.setWorkingdays(entityPackage.getWorkingdays());
		newEntity.setOutcomesalary(entityPackage.getOutcomesalary());
		newEntity = repositoryInterface.save(newEntity);
		return true;
	
}
public List<EntityPackageDecember> getAllD() {
	return repositoryInterface.findAll();
}

}