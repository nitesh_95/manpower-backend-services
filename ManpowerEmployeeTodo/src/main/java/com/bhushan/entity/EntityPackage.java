package com.bhushan.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Table(name = "todomanpower")
@Entity
public class EntityPackage {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name  = "id")
	private long id;
	@Column(name  = "date")
	private String date;
	@Column(name  = "todotask")
	private String todotask;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTodotask() {
		return todotask;
	}
	public void setTodotask(String todotask) {
		this.todotask = todotask;
	}
	@Override
	public String toString() {
		return "EntityPackage [id=" + id + ", date=" + date + ", todotask=" + todotask + "]";
	}
	
	
}