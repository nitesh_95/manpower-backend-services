package com.bhushan.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.entity.EntityPackageAugust;
import com.bhushan.repository.RepositoryInterface;
import com.bhushan.util.Response;
import com.bhushan.util.ResponseCodes;

@Service
public class Services {

	@Autowired
	private RepositoryInterface repositoryInterface;

	public Response saveData(EntityPackageAugust entityPackage) {
		Response response = new Response();
		EntityPackageAugust newEntity = new EntityPackageAugust();
		try {

			newEntity.setId(entityPackage.getId());
			newEntity.setCasualleave(entityPackage.getCasualleave());
			newEntity.setSickleave(entityPackage.getSickleave());
			newEntity.setWorkingdays(entityPackage.getWorkingdays());
			newEntity.setOutcomesalary(entityPackage.getOutcomesalary());
			newEntity = repositoryInterface.save(newEntity);

			if (newEntity == null) {
				response.setStatus(ResponseCodes.NOT_CREATE.getCode());
				response.setReason(ResponseCodes.NOT_CREATE.getReason());
			} else {
				response.setStatus(ResponseCodes.SUCCESS.getCode());
				response.setReason(ResponseCodes.SUCCESS.getReason());
			}
		} catch (Exception e) {
			response.setStatus(ResponseCodes.FAILED.getCode());
			response.setReason(ResponseCodes.FAILED.getReason());
		}
		return response;
	}

	public List<EntityPackageAugust> getAll() {
		return repositoryInterface.findAll();
	}

	public boolean updateAugust(EntityPackageAugust entityPackage) {
		
		EntityPackageAugust newEntity =new EntityPackageAugust();
		newEntity.setId(entityPackage.getId());
		newEntity.setPaymentstatus(entityPackage.getPaymentstatus());
		newEntity.setDateofpayment(entityPackage.getDateofpayment());
		newEntity.setEmpname(entityPackage.getEmpname());
		newEntity.setMonthlysalary(entityPackage.getMonthlysalary());
		newEntity.setCasualleave(entityPackage.getCasualleave());
		newEntity.setSickleave(entityPackage.getSickleave());
		newEntity.setWorkingdays(entityPackage.getWorkingdays());
		newEntity.setOutcomesalary(entityPackage.getOutcomesalary());
		newEntity = repositoryInterface.save(newEntity);
		return true;
	
}
	

	public boolean deleteId(Long id) {
		Optional<EntityPackageAugust> entity = repositoryInterface.findById(id);
		if (entity.isPresent()) {
			repositoryInterface.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	public EntityPackageAugust getArticleById(long articleId) {
		EntityPackageAugust obj = repositoryInterface.findById(articleId).get();
		return obj;
	}

	public List<EntityPackageAugust> saveAllStudent(List<EntityPackageAugust> studentList) {
		List<EntityPackageAugust> response = (List<EntityPackageAugust>) repositoryInterface.saveAll(studentList);
		return response;
	}
}

