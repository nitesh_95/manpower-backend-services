package com.bhushan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManpowerEmployeeLoginServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManpowerEmployeeLoginServiceApplication.class, args);
	}

}
