package com.bhushan.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Table(name = "employeemgmt")
@Entity
public class EntityPackage {

	@Id
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "adhar")
	
	
	private String adhar;
	@Column(name = "joineddate")
	private String joineddate;
	@Column(name = "pan")
	private String pan;
	@Column(name = "mobile")
	private String mobile;
	@Column(name = "address")
	private String address;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	
	public String getJoineddate() {
		return joineddate;
	}
	public void setJoineddate(String joineddate) {
		this.joineddate = joineddate;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdhar() {
		return adhar;
	}
	public void setAdhar(String adhar) {
		this.adhar = adhar;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "EntityPackage [id=" + id + ", name=" + name + ", adhar=" + adhar + ", pan=" + pan + ", mobile=" + mobile
				+ ", address=" + address + "]";
	}
	 
}
