package com.bhushan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bhushan.entity.EntityPackageSeptember;

@Repository
public interface RepositoryInterface2 extends JpaRepository<EntityPackageSeptember, Long>{

}
