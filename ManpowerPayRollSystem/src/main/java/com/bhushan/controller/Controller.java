package com.bhushan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.entity.EntityPackage;
import com.bhushan.entity.EntityPackageAugust;
import com.bhushan.entity.EntityPackageDecember;
import com.bhushan.entity.EntityPackageNovember;
import com.bhushan.entity.EntityPackageOctober;
import com.bhushan.entity.EntityPackageSeptember;
import com.bhushan.service.Services;
import com.bhushan.service.Services2;
import com.bhushan.service.Services3;
import com.bhushan.service.Services4;
import com.bhushan.service.Services5;
import com.bhushan.service.Services6;
import com.bhushan.util.Response;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin
public class Controller {
	@Autowired
	private Services services;
	@Autowired
	private Services2 services2;
	@Autowired
	private Services3 services3;
	@Autowired
	private Services4 services4;
	@Autowired
	private Services5 services5;
	@Autowired
	private Services6 services6;
	

	@RequestMapping("/save")
	@ApiOperation("Create Employee")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response save(@RequestBody EntityPackageAugust entityPackage) {
		Response res = new Response();
		
		try {
			res  = services.saveData(entityPackage);
		} catch (Exception e) {
			System.out.println(e);
		}
		return res;

	}
	@RequestMapping("/getAll")
	@ApiOperation("Get All Employee")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public List<EntityPackage> getLis() {
		return services6.getAll();
	}
	@RequestMapping("/getAll/august")
	@ApiOperation("Get All Employee")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public List<EntityPackageAugust> getList() {
		return services.getAll();
	}
	@RequestMapping("/getAll/september")
	@ApiOperation("Get All Employee")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public List<EntityPackageSeptember> getLists() {
		return services2.getAllS();
	}
	@RequestMapping("/getAll/october")
	@ApiOperation("Get All Employee")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public List<EntityPackageOctober> getListss() {
		return services3.getAllO();
	}
	@RequestMapping("/getAll/november")
	@ApiOperation("Get All Employee")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public List<EntityPackageNovember> getListsss() {
		return services4.getAllN();
	}
	@RequestMapping("/getAll/december")
	@ApiOperation("Get All Employee")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public List<EntityPackageDecember> getListssss() {
		return services5.getAllD();
	}
	

	@RequestMapping(value = "/saveall/august")
	public List<EntityPackageAugust> saveAllStudents(@RequestBody List<EntityPackageAugust> studentList) {
		List<EntityPackageAugust> studentResponse = (List<EntityPackageAugust>) services.saveAllStudent(studentList);
		return studentResponse;
	}
	
	@RequestMapping("/delete/{id}")
	@ApiOperation("Delete Employee By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response delete(@PathVariable("id") long id) {
		Response res = new Response();
		boolean deleted = services.deleteId(id);
		if (deleted) {
			res.setReason("Success");
			res.setStatus("00");
		} else {
			res.setReason("Failed");
			res.setStatus("s01");
		}
		return res;
	}
	@RequestMapping("/updateData")
	@ApiOperation("Update Employee By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response updateaugs(@RequestBody EntityPackage entityPackage) {
		Response res = new Response();
		boolean updated = services6.updateAugust(entityPackage);
		if (updated) {
			res.setReason("Success");
			res.setStatus("00");
		} else {
			res.setReason("Failed");
			res.setStatus("s01");
		}
		return res;

	}


	@RequestMapping("/updateData/August")
	@ApiOperation("Update Employee By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response updateaug(@RequestBody EntityPackageAugust entityPackage) {
		Response res = new Response();
		boolean updated = services.updateAugust(entityPackage);
		if (updated) {
			res.setReason("Success");
			res.setStatus("00");
		} else {
			res.setReason("Failed");
			res.setStatus("s01");
		}
		return res;

	}

	@RequestMapping("/updateData/september")
	@ApiOperation("Update Employee By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response updatesep(@RequestBody EntityPackageSeptember entityPackage) {
		Response res = new Response();
		boolean updated = services2.updateSeptember(entityPackage);
		if (updated) {
			res.setReason("Success");
			res.setStatus("00");
		} else {
			res.setReason("Failed");
			res.setStatus("s01");
		}
		return res;

	}

	@RequestMapping("/updateData/october")
	@ApiOperation("Update Employee By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response updateoct(@RequestBody EntityPackageOctober entityPackage) {
		Response res = new Response();
		boolean updated = services3.updateOctober(entityPackage);
		if (updated) {
			res.setReason("Success");
			res.setStatus("00");
		} else {
			res.setReason("Failed");
			res.setStatus("s01");
		}
		return res;

	}

	@RequestMapping("/updateData/november")
	@ApiOperation("Update Employee By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response updatenov(@RequestBody EntityPackageNovember entityPackage) {
		Response res = new Response();
		boolean updated = services4.updateNovember(entityPackage);
		if (updated) {
			res.setReason("Success");
			res.setStatus("00");
		} else {
			res.setReason("Failed");
			res.setStatus("s01");
		}
		return res;

	}

	@RequestMapping("/updateData/december")
	@ApiOperation("Update Employee By Id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "00-Success, 01-Internal Server error,"
			+ "02-No Data found," + "03-Query is Not updated", response = Response.class) })
	public Response updatedec(@RequestBody EntityPackageDecember entityPackage) {
		Response res = new Response();
		boolean updated = services5.updateDecember(entityPackage);
		if (updated) {
			res.setReason("Success");
			res.setStatus("00");
		} else {
			res.setReason("Failed");
			res.setStatus("s01");
		}
		return res;

	}

	

	@RequestMapping("/getEmployee/{id}")
	public ResponseEntity<EntityPackage> getArticleById(@PathVariable("id") Long id) {
		EntityPackage article = services6.getArticleById(id);
		return new ResponseEntity<EntityPackage>(article, HttpStatus.OK);
	}
}