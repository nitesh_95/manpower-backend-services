package com.bhushan.filedemo.service;

import com.bhushan.filedemo.exception.FileStorageException;
import com.bhushan.filedemo.exception.MyFileNotFoundException;
import com.bhushan.filedemo.model.DBFile;
import com.bhushan.filedemo.repository.DBFileRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Service
public class DBFileStorageService {

	@Autowired
	private DBFileRepository dbFileRepository;

	public DBFile storeFile(MultipartFile file) {
		LocalDate today = LocalDate.now();
		System.out.println("Current Date=" + today);
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}

			DBFile dbFile = new DBFile();
			dbFile.setFileName(fileName);
			dbFile.setFileType(file.getContentType());
			dbFile.setData(file.getBytes());
			dbFile.setDate(today);
			;

			return dbFileRepository.save(dbFile);
		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}


	public List<DBFile> getAll() {
		return dbFileRepository.findAll();
	}

	public DBFile getFile(String fileId) {
		return dbFileRepository.findById(fileId)
				.orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
	}

	public long countEntities() {
		long count = dbFileRepository.count();
		return count;
	}
}
